Saya membuat frontend dengan reactjs dan tambahan library pendukung lainnya seperti react-router-rom dll, saya juga menggunakan framework tailwindcss.
untuk dapat menjalankan project frontend:

1. buka terlebih dahulu project di visual studio code
2. ketikan yarn start (saya menggunakan yarn)

Saya membuat backend dengan nodejs dan express dengan database mysql.
untuk dapat menjalankan project backend:

1. buka terlebih dahulu project di visual studio code
2. ketikan nodemon index.js (saya menggunakan nodemon) di terminal atau dengan node index.js (pastikan koneksi ke database sudah terhubung)

Saya menggunakan tool pembantu untuk manajemen database yaitu MySQL Workbench.
untuk dapat menggunakan database:

1. nyalakan / aktifkan / connect-kan workbrench
2. masuk kedalam folder project backend lalu arahkan ke file index yang berada di ./config/database/index.js
3. ganti data connection yang bapak gunakan di workbench
4. untuk file database sudah tersedia di dalam folder project backend dengan nama privyid_db
5. untuk mengimportnya (saya menggunakan workbench) bikin database dengan nama privyid_db lalu pilih server (di menu atas) --> data import --> pilih file nya

- user sudah bisa registrasi
- user sudah bisa login dengan data registrasi, akan tetapi untuk menu / hal-hal yang dibutuhkan untuk user yang sudah login belum bisa.
- user sudah bisa mendapatkan kode OTP dan bisa mengirim ulang kode OTP dan memverifikasi nya
- user sudah bisa menampilkan data profile dan juga cover image, profile image
- user sudah bisa update data profile
- user sudah bisa upload dan mengganti serta menghapus cover image serta profile image
- user sudah bisa membaca, membuat, mengedit dan menghapus data riwayat kerja.
- user sudah bisa membaca, membuat, mengedit dan menghapus data riwayat pendidikan.
- user sudah bisa membaca image gallery yang tersimpan di backend
- user sudah bisa menghapus image gallery

- user masih tidak bisa mengupload image gallery dengan banyak gambar
- untuk login logout belum sempat diselesaikan.

- semua data sudah terhubung dengan backend dan database.

\*\* saya telah mencoba berbagai cara untuk dapat menggunakan swagger untuk dokumentasi rest api, akan tetapi saya mendapatkan error yang saya belum mendapatkan jawab yang pas untuk mengatasi error tersebut dan karena saya baru pertama kali menggunakan swagger.

Untuk pengujian REST API saya menggunakan postman dan hal-hal yang saya uji tersedia disini:
https://www.getpostman.com/collections/420d5a3d001079353012
