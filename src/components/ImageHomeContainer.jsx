import React from 'react';
import { FiEdit } from 'react-icons/fi';
import { Link } from 'react-router-dom';

function ImageHomeContainer({ propsCover, propsProfile }) {
  const imgCover = `http://localhost:2020/user/cover/${propsCover}`;
  const imgProfile = `http://localhost:2020/user/profile/${propsProfile}`;

  return (
    <div>
      <div className="relative">
        <img
          className="w-full h-60 bg-center bg-cover bg-no-repeat"
          src={imgCover}
          alt=""
        />
        <Link to="/editprofile">
          <button className="p-2 w-24 bg-white text-indigo-500 border border-indigo-500 rounded shadow absolute right-5 bottom-5">
            Edit
          </button>
        </Link>
      </div>
      <img
        className="sm:w-24 sm:h-24 md:w-32 md:h-32 w-52 h-52 rounded-full absolute sm:top-64 md:top-56 top-48 left-32"
        src={imgProfile}
        alt=""
      />
      <div className="w-9 h-9 bg-white rounded-full flex justify-center items-center absolute top-72 left-56">
        <Link to="/editprofile">
          <FiEdit />
        </Link>
      </div>
    </div>
  );
}

export default ImageHomeContainer;
