import React from 'react';

function OtpComp({ propsKodeOtp, propsKirimUlang, propsVerifikasiOtp }) {
  console.log('baba' + propsKodeOtp);
  return (
    <div className="w-vw h-screen flex justify-center items-center">
      <div className="w-96 h-80 flex flex-col items-center rounded bg-gray-200">
        <h1 className="font-semibold text-xl my-5">Percobaan OTP</h1>
        <div className="w-72 flex justify-around items-center my-5">
          <input
            className="w-12 h-12 text-center rounded border-2 border-gray-300"
            type="text"
            placeholder="X"
            defaultValue={propsKodeOtp[0]}
          />
          <input
            className="w-12 h-12 text-center rounded border-2 border-gray-300"
            type="text"
            placeholder="X"
            defaultValue={propsKodeOtp[1]}
          />
          <input
            className="w-12 h-12 text-center rounded border-2 border-gray-300"
            type="text"
            placeholder="X"
            defaultValue={propsKodeOtp[2]}
          />
          <input
            className="w-12 h-12 text-center rounded border-2 border-gray-300"
            type="text"
            placeholder="X"
            defaultValue={propsKodeOtp[3]}
          />
        </div>
        <button
          className="w-28 p-2 bg-indigo-500 text-white rounded shadow mb-3"
          onClick={propsVerifikasiOtp}>
          Verifikasi
        </button>
        <div className="w-80 border border-gray-500 my-4"></div>
        <button className="font-semibold underline" onClick={propsKirimUlang}>
          Kirim Ulang Kode OTP
        </button>
      </div>
    </div>
  );
}

export default OtpComp;
