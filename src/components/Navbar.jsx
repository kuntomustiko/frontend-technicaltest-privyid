import React, { useState } from 'react';
import { Link } from 'react-router-dom';

function Navbar() {
  const [open, setOpen] = useState(false);
  return (
    <div className="bg-gradient-to-br from-blue-500 to-blue-400">
      <div className="flex flex-col lg:flex-row ">
        <div className="w-full flex items-center justify-between  px-4 py-4 lg:py-0 border-b border-blue-300 lg:border-b-0">
          <Link to={`/`}>
            <a href="#" className="uppercase font-semibold text-white">
              Brand
            </a>
          </Link>

          <div className=" sm:hidden md:hidden lg:block xl:block 2xl:block lg:flex lg:flex-row lg:w-full xl:flex xl:flex-row xl:w-full 2xl:flex 2xl:flex-row 2xl:w-full justify-end  ">
            <Link to={`/login`} className="text-center block">
              <a
                href="#"
                className="block w-20 m-2 p-1 rounded border border-blue-500 text-white hover:bg-blue-500 hover:text-white">
                Login
              </a>
            </Link>
            <Link to={`/register`} className="text-center block">
              <a
                href="#"
                className="block w-20 m-2 p-1 rounded border border-blue-500 text-white hover:bg-blue-500 hover:text-white">
                Register
              </a>
            </Link>
          </div>

          <button
            // ketika klik-1 maka akan true (kebuka), klik-2 maka akan false(tutup)
            onClick={() => setOpen(!open)}
            className="focus:outline-none text-white block lg:hidden">
            <svg
              class="w-6 h-6"
              fill="none"
              stroke="currentColor"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg">
              {/* menu hamburger */}
              <path
                className={open == false ? 'block' : 'hidden'}
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M4 6h16M4 12h16M4 18h16"
              />
              {/* menu x */}
              <path
                className={open == true ? 'block' : 'hidden'}
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M6 18L18 6M6 6l12 12"
              />
            </svg>
          </button>
        </div>

        <div
          className={`${
            open ? 'block' : 'hidden'
          } lg:hidden flex-col lg:flex-row justify-between w-full py-4 lg:py-0`}>
          <div className="flex flex-col lg:flex-row">
            <Link to={`/login`} className="text-center block">
              <a
                href="#"
                className="block px-4 py-3 lg:py-5 text-rise-100 hover:text-white">
                Login
              </a>
            </Link>
            <Link to={`/register`} className="text-center block">
              <a
                href="#"
                className="block px-4 py-2 lg:py-5 text-blue-100 hover:text-white">
                Register
              </a>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Navbar;
