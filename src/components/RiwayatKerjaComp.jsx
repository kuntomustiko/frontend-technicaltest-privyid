import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function RiwayatKerjaComp({ propsDataRiwayatKerja, propsOnClickDelete }) {
  const [jumlahData, setJumlahData] = useState(false);

  useEffect(() => {
    if (propsDataRiwayatKerja.length >= 3) {
      setJumlahData(true);
    } else {
      setJumlahData(false);
    }
  }, [propsDataRiwayatKerja]);

  const renderList = () => {
    return propsDataRiwayatKerja.map((data, index) => {
      return (
        <div
          key={index}
          className="sm:w-72 md:w-52 md:h-32 lg:w-72 xl:w-96 lg:h-44 bg-gray-200 rounded shadow md:mt-2 mt-8 p-5">
          <h3 className="md:text-xl text-2xl font-semibold text-indigo-500 ">
            {data.jabatan}
          </h3>
          <h3 className="md:text-base text-lg font-semibold">
            {data.perusahaan}
          </h3>
          <p className="md:text-base text-lg">
            <span>{data.tahunsebelum}</span> - <span>{data.tahunsesudah}</span>
          </p>
          <p className="md:text-sm">{data.tempatkerja}</p>
          <div className="flex justify-between items-center m-2">
            <Link to={`/editriwayatkerja/${data.riwayatkerja_id}`}>
              <button className="p-2 w-24 border border-indigo-500 rounded shadow">
                Edit
              </button>
            </Link>
            <button
              className="p-2 w-24 border border-indigo-500 rounded shadow"
              onClick={() => {
                propsOnClickDelete(data.riwayatkerja_id);
              }}>
              Hapus
            </button>
          </div>
        </div>
      );
    });
  };

  return (
    <div className="sm:mt-5 md:m-5 lg:ml-24 ">
      <div className="flex justify-between items-center m-5 ">
        <h1 className="sm:text-lg md:text-xl text-2xl font-semibold">
          Riwayat Kerja
        </h1>

        <Link to={`/AddRiwayatPage/${1}`}>
          <button className="p-2 w-24 border border-indigo-500 rounded shadow">
            Tambah
          </button>
        </Link>
      </div>
      <div
        className={`sm:w-72 md:w-52 lg:w-72 xl:w-96 h-96 ${
          jumlahData ? 'overflow-y-scroll' : ''
        }`}>
        {renderList()}
      </div>
    </div>
  );
}

export default RiwayatKerjaComp;
