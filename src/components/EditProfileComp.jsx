import React from 'react';
import { Link } from 'react-router-dom';

function EditProfileComp(props) {
  return (
    <div className="flex justify-center items-center w-screen">
      <div className="sm:w-56 md:w-80 xl:w-5/6 h-auto border border-blue-500 rounded shadow p-5">
        <div className="text-center my-5 ">
          <h1 className="text-3xl font-semibold">Profile</h1>
        </div>
        <div className="flex flex-col justify-center items-center border border-black">
          {/* cover */}
          <img
            className="w-full h-60 bg-center bg-cover bg-no-repeat"
            src={props.propsCoverImageData}
            alt=""
          />
          <input
            type="file"
            ref={props.propsCoverImageRef}
            onChange={props.propsChangeImageCover}
          />
          <div className="flex sm:flex-col justify-around items-center w-80 my-5">
            <button
              className="mt-5 sm:w-20 md:w-36 p-2 border border-gray-500 rounded shadow hover:bg-transparent hover:text-black"
              onClick={props.propsOnClickHapusCover}>
              hapus
            </button>
            <button
              className="mt-5 sm:w-20 md:w-36 p-2 bg-gray-400 rounded shadow text-white hover:bg-transparent hover:text-black"
              onClick={props.propsOnClickUploadCover}>
              upload
            </button>
          </div>
        </div>
        <div className="flex flex-col justify-center items-center my-5">
          {/* profile */}
          <img
            className="sm:w-24 sm:h-24 md:w-32 md:h-32 w-52 h-52 rounded-full "
            src={props.propsProfileImageData}
            alt=""
          />
          <input
            type="file"
            ref={props.propsProfileImageRef}
            onChange={props.propsChangeImageProfile}
          />
          <div className="flex sm:flex-col justify-around items-center w-80">
            <button
              className="mt-5 sm:w-20 md:w-36 p-2 border border-gray-500 rounded shadow hover:bg-transparent hover:text-black"
              onClick={props.propsOnClickHapusProfile}>
              hapus
            </button>
            <button
              className="mt-5 sm:w-20 md:w-36 p-2 bg-gray-400 rounded shadow text-white hover:bg-transparent hover:text-black"
              onClick={props.propsOnClickUploadProfile}>
              upload
            </button>
          </div>
        </div>

        <div className="m-3">
          <h4>Username</h4>
        </div>
        <form className="m-3">
          <input
            className="sm:w-40 md:w-5/6 w-80 border border-blue-200 shadow rounded p-1"
            placeholder="username"
            type="text"
            ref={props.propsUsernameRef}
            defaultValue={props.propsUsernameDefault}
          />
        </form>
        <div className="m-3">
          <h4>Email</h4>
        </div>
        <form className="m-3">
          <input
            className="sm:w-40 md:w-5/6 w-80 border border-blue-200 shadow rounded p-1"
            placeholder="text"
            type="email"
            ref={props.propsEmailRef}
            defaultValue={props.propsEmailDefault}
          />
        </form>
        <div className="m-3">
          <h4>Contact Number</h4>
        </div>
        <form className="m-3">
          <input
            className="sm:w-40 md:w-5/6 w-80 border border-blue-200 shadow rounded p-1"
            placeholder="Phone"
            type="number"
            ref={props.propsPhoneNumberRef}
            defaultValue={props.propsPhoneNumberDefault}
          />
        </form>
        <div className="flex md:w-5/6 items-center justify-center m-5">
          <Link to="/">
            <button className="sm:w-48 md:w-36 p-2 border-red-400 text-red-500 border rounded shadow hover:bg-transparent hover:text-black mx-5">
              Batal
            </button>
          </Link>
          <button
            className="sm:w-48 md:w-36 p-2 bg-blue-400 rounded shadow text-white hover:bg-transparent hover:text-black mx-5"
            onClick={props.propsOnClickSimpan}>
            Simpan
          </button>
        </div>
      </div>
    </div>
  );
}

export default EditProfileComp;
