import React from 'react';
import { Link } from 'react-router-dom';

function RegisterComp(props) {
  return (
    <div className="flex justify-center items-center h-screen w-screen">
      <div className="sm:w-56 md:w-80 w-96 h-4/5 border border-blue-500 rounded shadow">
        <div className="text-center my-5">
          <h1 className="text-3xl font-semibold">Register</h1>
        </div>
        <div className="m-3">
          <h4>Phone</h4>
        </div>
        <form className="m-3">
          <input
            className="sm:w-48 md:w-72 w-80 border border-blue-200 shadow rounded p-1"
            placeholder="Phone"
            type="text"
            ref={props.propsPhoneRef}
          />
        </form>

        <div className="m-3">
          <h4>Password</h4>
        </div>
        <form className="m-3">
          <input
            className="sm:w-48 md:w-72 w-80 border border-blue-200 shadow rounded p-1"
            placeholder="password"
            type="password"
            ref={props.propsPasswordRef}
          />
        </form>
        <div className="m-3">
          <h4>Country</h4>
        </div>
        <form className="m-3">
          <input
            className="sm:w-48 md:w-72 w-80 border border-blue-200 shadow rounded p-1"
            placeholder="Country"
            type="email"
            ref={props.propsCountryRef}
          />
        </form>
        <div className="flex items-center justify-center ">
          <button
            className="sm:w-48 md:w-72 p-1 bg-blue-400 rounded shadow text-white hover:bg-transparent hover:text-black"
            onClick={props.propsOnClickRegister}>
            Register
          </button>
        </div>
        <p className="sm:text-xs md:text-sm text-center my-3">
          Sudah punya account ?
          <Link to="/login">
            <span className="text-sm sm:text-xs md:text-sm  text-blue-500 ml-2 hover:underline">
              Login
            </span>
          </Link>
        </p>
      </div>
    </div>
  );
}

export default RegisterComp;
