import React, { useState, useEffect } from 'react';
import axios from '../config/axios/index';
import RiwayatKerjaComp from './RiwayatKerjaComp';

function RiwayatKerjaContainer() {
  const [riwayatKerja, setRiwayatKerja] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    axios.get(`/riwayatkerja/all/${1}`).then((res) => {
      setRiwayatKerja(res.data);
    });
  };

  const onClickDeleteRiwayatKerja = (id) => {
    axios
      .delete(`/riwayatkerja/delete/${id}`)
      .then((res) => {
        getData();
      })
      .catch((error) => console.log(error));
  };

  return (
    <RiwayatKerjaComp
      propsDataRiwayatKerja={riwayatKerja}
      propsOnClickDelete={onClickDeleteRiwayatKerja}
    />
  );
}

export default RiwayatKerjaContainer;
