import React from 'react';
import { Link } from 'react-router-dom';

function AddRiwayatPendidikanComp(props) {
  return (
    <div className="flex justify-center items-center h-screen w-screen">
      <div className="sm:w-56 md:w-80 xl:w-96 h-auto border border-blue-500 rounded shadow">
        <div className="text-center my-5">
          <h1 className="text-3xl font-semibold">Riwayat Pendidikan</h1>
        </div>
        <div className="m-3">
          <h4>Gelar</h4>
        </div>
        <form className="m-3">
          <input
            className="sm:w-48 md:w-5/6 w-80 border border-blue-200 shadow rounded p-1"
            placeholder="text"
            type="text"
            ref={props.propsNamaGelarRef}
          />
        </form>
        <div className="m-3">
          <h4>Tempat Pendidikan</h4>
        </div>
        <form className="m-3">
          <input
            className="sm:w-48 md:w-5/6 w-80 border border-blue-200 shadow rounded p-1"
            placeholder="text"
            type="text"
            ref={props.propsTempatPendidikanRef}
          />
        </form>
        <div className="m-3">
          <h4>Tahun Pendidikan</h4>
        </div>
        <form className="m-3">
          <input
            className="sm:w-48 md:w-5/6 w-80 border border-blue-200 shadow rounded p-1"
            placeholder="masa sebelum"
            type="text"
            ref={props.propsTahunSebelumRef}
          />
          <p> - </p>
          <input
            className="sm:w-48 md:w-5/6 w-80 border border-blue-200 shadow rounded p-1"
            placeholder="masa sesudah"
            type="text"
            ref={props.propsTahunSesudahRef}
          />
        </form>
        <div className="flex md:w-5/6 items-center justify-around mb-5">
          <Link to="/">
            <button className="sm:w-48 md:w-36 p-2 border-red-400 text-red-500 border rounded shadow hover:bg-transparent hover:text-black">
              Batal
            </button>
          </Link>
          <button
            className="sm:w-48 md:w-36 p-2 bg-blue-400 rounded shadow text-white hover:bg-transparent hover:text-black"
            onClick={props.propsOnClickSimpan}>
            Simpan
          </button>
        </div>
      </div>
    </div>
  );
}

export default AddRiwayatPendidikanComp;
