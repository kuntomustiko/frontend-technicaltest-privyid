import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from '../config/axios/index';
import GalleryItemComp from '../components/GalleryItemComp';

function GalleryContainer() {
  const [dataGallery, setDataGallery] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  // ambil id dari user yang masuk
  const getData = () => {
    axios.get(`/gallery/all/${1}`).then((res) => {
      setDataGallery(res.data);
      console.log(res.data);
    });
  };

  const onClickDeleteGallery = (id) => {
    console.log(id);
    axios
      .delete(`/gallery/delete/${id}`)
      .then((res) => {
        getData();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <Link to={`/AddGalleryPage`}>
        <button className="p-2 w-24 border border-indigo-500 rounded shadow">
          Tambah
        </button>
      </Link>
      <div className="sm:mx-auto sm:justify-center h-5/6 sm:mt-5 flex xl:flex-row md:justify-between items-center content-start flex-wrap bg-blue-300 overflow-y-scroll p-3">
        <GalleryItemComp
          propsDataGallery={dataGallery}
          propsOnClickDeleteGallery={onClickDeleteGallery}
        />
      </div>
    </div>
  );
}

export default GalleryContainer;
