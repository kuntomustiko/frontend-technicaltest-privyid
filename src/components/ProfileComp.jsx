import React from 'react';
import { Link } from 'react-router-dom';

function ProfileComp({ propsUsername, propsEmail, propsPhoneNumber }) {
  return (
    <div className="sm:mt-20 m-0 lg:w-72 mt-32 p-5 w-80 h-72 bg-gray-300 rounded shadow ">
      <h2 className="text-center text-2xl font-semibold">Profile</h2>
      <div className="my-2">
        <h3 className="text-lg">Username :</h3>
        <p>{propsUsername}</p>
      </div>
      <div className="my-2">
        <h3 className="text-lg">Email :</h3>
        <p>{propsEmail}</p>
      </div>
      <div className="my-2">
        <h3 className="text-lg">Contact Number :</h3>
        <p>{propsPhoneNumber}</p>
      </div>
      <div className="flex justify-center items-center m-2">
        <Link to="/editprofile">
          <button className="p-2 w-24 border border-indigo-500 rounded shadow">
            Edit
          </button>
        </Link>
      </div>
    </div>
  );
}

export default ProfileComp;
