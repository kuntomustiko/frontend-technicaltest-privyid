import React from 'react';
import { BsTrash } from 'react-icons/bs';

function GalleryItemComp({ propsDataGallery, propsOnClickDeleteGallery }) {
  return propsDataGallery.map((data, index) => {
    return (
      <div className="relative shadow sm:mx-5 md:mx-0 my-3 w-56">
        <img
          className="w-56 h-48 rounded-xl"
          src={`http://localhost:2020/gallery/${data.gallery_image}`}
          alt=""
        />
        <div className="w-9 h-9 bg-white rounded-full flex justify-center items-center absolute top-3 right-3">
          <button onClick={() => propsOnClickDeleteGallery(data.gallery_id)}>
            <BsTrash />
          </button>
        </div>
      </div>
    );
  });
}

export default GalleryItemComp;
