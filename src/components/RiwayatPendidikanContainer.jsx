import React, { useState, useEffect } from 'react';
import axios from '../config/axios/index';
import RiwayatPendidikanComp from './RiwayatPendidikanComp';

function RiwayatPendidikanContainer() {
  const [riwayatPendidikan, setRiwayatPendidikan] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    axios.get(`/riwayatpendidikan/all/${1}`).then((res) => {
      setRiwayatPendidikan(res.data);
    });
  };

  const onClickDeleteRiwayatPendidikan = (id) => {
    axios
      .delete(`/riwayatpendidikan/delete/${id}`)
      .then((res) => {
        getData();
      })
      .catch((error) => console.log(error));
  };

  return (
    <RiwayatPendidikanComp
      propsDataRiwayatPendidikan={riwayatPendidikan}
      propsOnClickDelete={onClickDeleteRiwayatPendidikan}
    />
  );
}

export default RiwayatPendidikanContainer;
