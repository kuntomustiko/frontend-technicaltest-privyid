import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function RiwayatPendidikanComp({
  propsDataRiwayatPendidikan,
  propsOnClickDelete,
}) {
  const [jumlahData, setJumlahData] = useState(false);

  useEffect(() => {
    if (propsDataRiwayatPendidikan.length >= 3) {
      setJumlahData(true);
    } else {
      setJumlahData(false);
    }
  }, [propsDataRiwayatPendidikan]);

  const renderList = () => {
    return propsDataRiwayatPendidikan.map((data, index) => {
      return (
        <div
          key={index}
          className="sm:w-72 md:w-52 md:h-32 lg:w-72 xl:w-96 lg:h-44 bg-gray-200 rounded shadow md:mt-2 mt-8 p-5">
          <h3 className="md:text-xl text-2xl font-semibold text-indigo-500 ">
            {data.gelar}
          </h3>
          <h3 className="md:text-base text-lg font-semibold">
            {data.tempatpendidikan}
          </h3>
          <p className="md:text-base text-lg">
            <span>{data.tahunsebelum}</span> - <span>{data.tahunsesudah}</span>
          </p>
          <div className="flex justify-between items-center m-2">
            <Link to={`/editriwayatpendidikan/${data.riwayatpendidikan_id}`}>
              <button className="p-2 w-24 border border-indigo-500 rounded shadow">
                Edit
              </button>
            </Link>
            <button
              className="p-2 w-24 border border-indigo-500 rounded shadow"
              onClick={() => {
                propsOnClickDelete(data.riwayatkerja_id);
              }}>
              Hapus
            </button>
          </div>
        </div>
      );
    });
  };

  return (
    <div className="sm:mt-5 lg:mx-5">
      <div className="flex justify-between items-center m-2 ">
        <h1 className="sm:text-lg md:text-xl text-2xl font-semibold m-2">
          Riwayat Pendidikan
        </h1>
        <Link to={`/AddRiwayatPage/${0}`}>
          <button className="p-2 w-24 border border-indigo-500 rounded shadow">
            Tambah
          </button>
        </Link>
      </div>

      <div
        className={`sm:w-72 md:w-52 lg:w-72 xl:w-96 h-96 ${
          jumlahData ? 'overflow-y-scroll' : ''
        }`}>
        {renderList()}
      </div>
    </div>
  );
}

export default RiwayatPendidikanComp;
