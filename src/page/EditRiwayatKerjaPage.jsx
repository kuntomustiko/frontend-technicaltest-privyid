import React, { useRef, useState, useEffect } from 'react';
import EditRiwayatKerjaComp from '../components/EditRiwayatKerjaComp';
import { useParams } from 'react-router';
import axios from '../config/axios/index';

function EditRiwayatKerjaPage() {
  const namaJabatanRef = useRef();
  const namaPerusahaanRef = useRef();
  const masaSebelumRef = useRef();
  const masaSesudahRef = useRef();
  const tempatKerjaRef = useRef();

  const [dataRiwayatKerja, setDataRiwayatKerja] = useState([]);

  const { editriwayatkerja_id } = useParams();

  const {
    jabatan,
    perusahaan,
    tahunsebelum,
    tahunsesudah,
    tempatkerja,
  } = dataRiwayatKerja;

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    axios.get(`/riwayatkerja/readedited/${editriwayatkerja_id}`).then((res) => {
      setDataRiwayatKerja(res.data[0]);
    });
  };

  const onClickSimpan = () => {
    let _namaJabatanRef = namaJabatanRef.current.value;
    let _namaPerusahaanRef = namaPerusahaanRef.current.value;
    let _masaSebelumRef = masaSebelumRef.current.value;
    let _masaSesudahRef = masaSesudahRef.current.value;
    let _tempatKerjaRef = tempatKerjaRef.current.value;

    const body = {
      riwayatkerja_id: dataRiwayatKerja.riwayatkerja_id,
      users_id: dataRiwayatKerja.users_id,
      jabatan: _namaJabatanRef,
      perusahaan: _namaPerusahaanRef,
      tahunsebelum: _masaSebelumRef,
      tahunsesudah: _masaSesudahRef,
      tempatkerja: _tempatKerjaRef,
    };

    axios
      .patch(`/riwayatkerja/update/${editriwayatkerja_id}`, body)
      .then((res) => {
        if (res.status == 200) {
          window.location = '/';
        }
      })
      .catch((err) => console.log({ err }));
  };

  return (
    <EditRiwayatKerjaComp
      propsNamaJabatanRef={namaJabatanRef}
      propsNamaPerusahaanRef={namaPerusahaanRef}
      propsMasaSebelumRef={masaSebelumRef}
      propsMasaSesudahRef={masaSesudahRef}
      propsTempatKerjaRef={tempatKerjaRef}
      propsOnClickSimpan={onClickSimpan}
      propsJabatanDefault={jabatan}
      propsPerusahaanDefault={perusahaan}
      propsMasaSebelumDefault={tahunsebelum}
      propsMasaSesudahDefault={tahunsesudah}
      propsTempatDefault={tempatkerja}
    />
  );
}

export default EditRiwayatKerjaPage;
