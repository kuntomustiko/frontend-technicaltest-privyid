import React from 'react';
import Navbar from '../components/Navbar';

function HomeNonLoginPage() {
  return (
    <div>
      <Navbar />
      <div className="w-vw h-screen">
        <div className="h-screen flex justify-center items-center ">
          <div className="row w-80 h-30 text-center border border-blue-500 rounded shadow p-5">
            <h1 className="text-4xl font-bold my-3">Selamat Datang</h1>
            <h2 className="text-3xl font-semibold my-3">Technical Test</h2>
            <p className="my-3">Silahkan login terlebih dahulu</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HomeNonLoginPage;
