import React, { useState, useRef } from 'react';
import AddRiwayatKerjaComp from '../components/AddRiwayatKerjaComp';
import axios from '../config/axios/index';
import { useParams } from 'react-router-dom';
import AddRiwayatPendidikanComp from '../components/AddRiwayatPendidikanComp';

function AddRiwayatPage() {
  const [userId, setUserId] = useState(0);

  const { id } = useParams();

  const namaJabatanRef = useRef();
  const namaPerusahaanRef = useRef();
  const masaSebelumRef = useRef();
  const masaSesudahRef = useRef();
  const tempatKerjaRef = useRef();

  const namaGelarRef = useRef();
  const tahunSebelumRef = useRef();
  const tahunSesudahRef = useRef();
  const tempatPendidikanRef = useRef();

  const onClickSimpanRiwayatKerja = () => {
    let _namaJabatanRef = namaJabatanRef.current.value;
    let _namaPerusahaanRef = namaPerusahaanRef.current.value;
    let _masaSebelumRef = masaSebelumRef.current.value;
    let _masaSesudahRef = masaSesudahRef.current.value;
    let _tempatKerjaRef = tempatKerjaRef.current.value;

    const body = {
      users_id: 1,
      jabatan: _namaJabatanRef,
      perusahaan: _namaPerusahaanRef,
      tahunsebelum: _masaSebelumRef,
      tahunsesudah: _masaSesudahRef,
      tempatkerja: _tempatKerjaRef,
    };

    axios
      .post(`/riwayatkerja/adddata`, body)
      .then((res) => {
        if (res.status == 201) {
          window.location = '/';
        }
      })
      .catch((err) => console.log({ err }));
  };

  const onClickSimpanRiwayatPendidikan = () => {
    let _namaGelarRef = namaGelarRef.current.value;
    let _tahunSebelumRef = tahunSebelumRef.current.value;
    let _tahunSesudahRef = tahunSesudahRef.current.value;
    let _tempatPendidikanRef = tempatPendidikanRef.current.value;

    const body = {
      users_id: 1,
      gelar: _namaGelarRef,
      tempatpendidikan: _tempatPendidikanRef,
      tahunsebelum: _tahunSebelumRef,
      tahunsesudah: _tahunSesudahRef,
    };

    axios
      .post(`/riwayatpendidikan/adddata`, body)
      .then((res) => {
        if (res.status == 201) {
          window.location = '/';
        }
        console.log(res);
      })
      .catch((err) => console.log({ err }));
  };
  const renderComponent = () => {
    if (id == 1) {
      return (
        <AddRiwayatKerjaComp
          propsNamaJabatanRef={namaJabatanRef}
          propsNamaPerusahaanRef={namaPerusahaanRef}
          propsMasaSebelumRef={masaSebelumRef}
          propsMasaSesudahRef={masaSesudahRef}
          propsTempatKerjaRef={tempatKerjaRef}
          propsOnClickSimpan={onClickSimpanRiwayatKerja}
        />
      );
    } else {
      return (
        <AddRiwayatPendidikanComp
          propsNamaGelarRef={namaGelarRef}
          propsTempatPendidikanRef={tempatPendidikanRef}
          propsTahunSebelumRef={tahunSebelumRef}
          propsTahunSesudahRef={tahunSesudahRef}
          propsOnClickSimpan={onClickSimpanRiwayatPendidikan}
        />
      );
    }
  };

  return <>{renderComponent()}</>;
}

export default AddRiwayatPage;
