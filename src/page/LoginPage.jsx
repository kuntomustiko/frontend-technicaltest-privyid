import React, { useRef } from 'react';
import LoginComp from '../components/LoginComp';

import { useDispatch, useSelector } from 'react-redux';
import axios from '../config/axios';

import { loginAction } from '../config/redux/actions';

function LoginPage() {
  const phoneRef = useRef();
  const passwordRef = useRef();

  const dispatch = useDispatch();

  const onClickLogin = () => {
    const password = passwordRef.current.value;
    const phone = phoneRef.current.value;

    console.log(typeof password);
    console.log(typeof phone);

    const body = {
      phone_number: phone,
      password: password,
    };
    console.log(body);

    axios
      .post('/user/login', body)
      .then((res) => {
        let id = res.data.user.id;
        let phone_number = res.data.user.phone_number;
        let token = res.data.token;
        dispatch(loginAction({ id, phone_number, token }));
        if (res.status == 200) {
          window.location = '/otp';
        }
      })
      .catch((err) => console.log(err));
  };
  return (
    <LoginComp
      propsOnClickLogin={onClickLogin}
      propsPhoneRef={phoneRef}
      propsPasswordRef={passwordRef}
    />
  );
}

export default LoginPage;
