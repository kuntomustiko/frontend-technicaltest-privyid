import React, { useRef, useState, useEffect } from 'react';
import EditRiwayatPendidikanComp from '../components/EditRiwayatPendidikanComp';
import { useParams } from 'react-router';
import axios from '../config/axios/index';

function EditRiwayatPendidikanPage() {
  const namaGelarRef = useRef();
  const tempatPendidikanRef = useRef();
  const tahunSebelumRef = useRef();
  const tahunSesudahRef = useRef();

  const { editriwayatpendidikan_id } = useParams();

  const [dataRiwayatPendidikan, setDataRiwayatPendidikan] = useState([]);
  const {
    gelar,
    tempatpendidikan,
    tahunsebelum,
    tahunsesudah,
  } = dataRiwayatPendidikan;

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    axios
      .get(`/riwayatpendidikan/readedited/${editriwayatpendidikan_id}`)
      .then((res) => {
        setDataRiwayatPendidikan(res.data[0]);
      });
  };

  const onClickSimpan = () => {
    let _namaGelarRef = namaGelarRef.current.value;
    let _namaPendidikanRef = tempatPendidikanRef.current.value;
    let _tahunSebelumRef = tahunSebelumRef.current.value;
    let _tahunSesudahRef = tahunSesudahRef.current.value;

    const body = {
      riwayatpendidikan_id: dataRiwayatPendidikan.riwayatpendidikan_id,
      users_id: dataRiwayatPendidikan.users_id,
      gelar: _namaGelarRef,
      tempatpendidikan: _namaPendidikanRef,
      tahunsebelum: _tahunSebelumRef,
      tahunsesudah: _tahunSesudahRef,
    };

    axios
      .patch(`/riwayatpendidikan/update/${editriwayatpendidikan_id}`, body)
      .then((res) => {
        if (res.status == 200) {
          window.location = '/';
        }
      })
      .catch((err) => console.log({ err }));
  };

  return (
    <EditRiwayatPendidikanComp
      propsNamaGelarRef={namaGelarRef}
      propsTempatPendidikanRef={tempatPendidikanRef}
      propsTahunSebelumRef={tahunSebelumRef}
      propsTahunSesudahRef={tahunSesudahRef}
      propsOnClickSimpan={onClickSimpan}
      propsGelarDefault={gelar}
      propsTempatDefault={tempatpendidikan}
      propsTahunSebelumDefault={tahunsebelum}
      propsTahunSesudahDefault={tahunsesudah}
    />
  );
}

export default EditRiwayatPendidikanPage;
