import React, { useRef } from 'react';
import axios from '../config/axios';

import RegisterComp from '../components/RegisterComp';

function RegisterPage() {
  const phoneRef = useRef();
  const countryRef = useRef();
  const passwordRef = useRef();

  const onClickRegister = () => {
    const phone = phoneRef.current.value;
    const country = countryRef.current.value;
    const password = passwordRef.current.value;

    const data = {
      phone_number: phone,
      password: password,
      country: country,
    };

    axios
      .post('/register', data)
      .then((res) => {
        if (res.status == 200) {
          window.location = '/login';
        }
      })
      .catch((err) => console.log(err));
  };

  return (
    <RegisterComp
      propsOnClickRegister={onClickRegister}
      propsPhoneRef={phoneRef}
      propsPasswordRef={passwordRef}
      propsCountryRef={countryRef}
    />
  );
}

export default RegisterPage;
