import React, { useRef, useState, useEffect } from 'react';
import EditProfileComp from '../components/EditProfileComp';
import axios from '../config/axios/index';

function EditProfilePage() {
  const usernameRef = useRef();
  const emailRef = useRef();
  const phoneNumberRef = useRef();
  const coverImageRef = useRef();
  const profileImageRef = useRef();

  const [dataProfile, setDataProfile] = useState({});
  const [coverImageData, setCoverImageData] = useState('');
  const [profileImageData, setProfileImageData] = useState('');
  const { username, email, phone_number } = dataProfile;

  useEffect(() => {
    getDataUsers();
  }, []);

  const getDataUsers = () => {
    axios
      .get(`/users/${1}`)
      .then((res) => {
        const imgCover = `http://localhost:2020/user/cover/${res.data[0].cover_image}`;
        setCoverImageData(imgCover);
        const imgProfile = `http://localhost:2020/user/profile/${res.data[0].profile_image}`;
        setProfileImageData(imgProfile);
        setDataProfile(res.data[0]);
        console.log(res.data[0]);
      })
      .catch((err) => console.log(err));
  };

  const onClickUploadCover = () => {
    const body = new FormData();

    let picture = coverImageRef.current.files[0];

    body.append('cover_image', picture);
    // const config = { headers: { Authorization: token } };
    // id dari user yang login
    axios
      .post(`/user/cover/${1}`, body)
      .then((res) => console.log('Gambar Product Berhasil diubah', res))
      .catch((err) => console.log(err));
  };

  const onClickHapusCover = () => {
    const body = {
      cover_image: 'cover-default.png',
    };

    axios
      .patch(`/user/coverdefault/${1}`, body)
      .then((res) => {
        console.log(res);
        getDataUsers();
      })
      .catch((err) => console.log({ err }));
  };

  const onClickUploadProfile = () => {
    const body = new FormData();

    let picture = profileImageRef.current.files[0];

    body.append('profile_image', picture);
    axios
      .post(`/user/profile/${1}`, body)
      .then((res) => console.log('Gambar Product Berhasil diubah', res))
      .catch((err) => console.log(err));
  };

  const onClickHapusProfile = () => {
    const body = {
      profile_image: 'profile-default.png',
    };
    // dapat id user dari siapa yang sudah login
    axios
      .patch(`/user/profiledefault/${1}`, body)
      .then((res) => {
        console.log(res);
        getDataUsers();
      })
      .catch((err) => console.log({ err }));
  };

  const changeImageCover = (e) => {
    setCoverImageData(URL.createObjectURL(e.target.files[0]));
  };
  const changeImageProfile = (e) => {
    setProfileImageData(URL.createObjectURL(e.target.files[0]));
  };

  const onClickSimpan = () => {
    const body = {
      username: usernameRef.current.value,
      email: emailRef.current.value,
      phone_number: phoneNumberRef.current.value,
    };

    axios
      .patch(`/users/editusers/${1}`, body)
      .then((res) => {
        console.log(res);
        getDataUsers();
      })
      .catch((err) => console.log({ err }));
  };

  return (
    <EditProfileComp
      propsUsernameRef={usernameRef}
      propsEmailRef={emailRef}
      propsPhoneNumberRef={phoneNumberRef}
      propsCoverImageRef={coverImageRef}
      propsProfileImageRef={profileImageRef}
      propsCoverImageData={coverImageData}
      propsProfileImageData={profileImageData}
      propsChangeImageCover={changeImageCover}
      propsChangeImageProfile={changeImageProfile}
      propsOnClickSimpan={onClickSimpan}
      propsOnClickUploadCover={onClickUploadCover}
      propsOnClickHapusCover={onClickHapusCover}
      propsOnClickUploadProfile={onClickUploadProfile}
      propsOnClickHapusProfile={onClickHapusProfile}
      propsUsernameDefault={username}
      propsEmailDefault={email}
      propsPhoneNumberDefault={phone_number}
    />
  );
}

export default EditProfilePage;
