import React, { useEffect, useState } from 'react';
import Navbar from '../components/Navbar';

import RiwayatKerjaContainer from '../components/RiwayatKerjaContainer';
import RiwayatPendidikanContainer from '../components/RiwayatPendidikanContainer';
import ProfileComp from '../components/ProfileComp';
import GalleryContainer from '../components/GalleryContainer';
import ImageHomeContainer from '../components/ImageHomeContainer';
import axios from '../config/axios/index';

import HomeNonLoginPage from './HomeNonLoginPage';

function HomePage() {
  const [data, setData] = useState({});

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    axios
      .get(`/users/${1}`)
      .then((res) => {
        setData(res.data[0]);
        console.log(res.data[0]);
      })
      .catch((err) => console.log(err));
  };

  return (
    <>
      <Navbar />
      <ImageHomeContainer
        propsCover={data.cover_image}
        propsProfile={data.profile_image}
      />
      <div className="w-11/12 h-screen mx-auto">
        <div className="flex sm:flex-col md:flex-row justify-between items-center">
          <ProfileComp
            propsUsername={data.username}
            propsEmail={data.email}
            propsPhoneNumber={data.phone_number}
          />
          <RiwayatKerjaContainer />
          <RiwayatPendidikanContainer />
        </div>
        <GalleryContainer />
      </div>
    </>
  );
}

export default HomePage;
