import React, { useState } from 'react';
import axios from '../config/axios/index';
function AddGalleryPage() {
  const [coba, setCoba] = useState([]);
  const onClickUpload = () => {
    // const body = new FormData();
    // let picture = coverImageRef.current.files[0];
    // body.append('cover_image', picture);

    // axios
    //   .post(`/user/cover/${1}`, body)
    //   .then((res) => console.log('Gambar Product Berhasil diubah', res))
    //   .catch((err) => console.log(err));
    console.log(coba[0]);
  };

  const changeImage = (e) => {
    if (e.target.files) {
      const fileArray = Array.from(e.target.files).map(
        (file) => URL.createObjectURL(file)
        // setCoba(URL.createObjectURL(file))
      );
      console.log(fileArray[0]);
      setCoba(fileArray);
    }

    // setCoverImageData(URL.createObjectURL(e.target.files[0]));
  };
  return (
    <div>
      <div className="flex flex-col justify-center items-center border border-black">
        {/* cover */}
        <img
          className="w-full h-60 bg-center bg-cover bg-no-repeat"
          // src={props.propsCoverImageData}
          // alt=""
        />
        <input
          type="file"
          multiple
          // ref={props.propsCoverImageRef}
          // onChange={props.propsChangeImageCover}
          onChange={changeImage}
        />
        <div className="flex sm:flex-col justify-around items-center w-80 my-5">
          <button
            className="mt-5 sm:w-20 md:w-36 p-2 bg-gray-400 rounded shadow text-white hover:bg-transparent hover:text-black"
            // onClick={props.propsOnClickUploadCover}
            onClick={onClickUpload}>
            upload
          </button>
        </div>
      </div>
    </div>
  );
}

export default AddGalleryPage;
