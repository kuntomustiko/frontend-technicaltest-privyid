import React, { useEffect, useState } from 'react';
import OtpComp from '../components/OtpComp';
import axios from '../config/axios';

function OtpPage() {
  const [kodeOtp, setKodeOtp] = useState(0);
  useEffect(() => {
    getOtp();
  }, []);

  const getOtp = () => {
    axios
      .patch(`/user/kirimotp/${1}`)
      .then((res) => {
        let kodeOtpRespon = 0;
        kodeOtpRespon = res.data;
        console.log(kodeOtpRespon);
        var output = [];
        var otpSplit = kodeOtpRespon.toString();

        for (var i = 0, len = otpSplit.length; i < len; i += 1) {
          output.push(+otpSplit.charAt(i));
        }
        setKodeOtp(output);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onClickKirimUlangOtp = () => {
    getOtp();
  };

  const onClickVerifikasiOtp = () => {
    var onlykode = kodeOtp.join('');
    console.log(onlykode);
    axios
      .get(`/user/cocokotp/${onlykode}`)
      .then((res) => {
        console.log(res);
        if (res.status == 200) {
          window.location = '/';
        }
      })
      .catch((err) => console.log(err));
  };
  return (
    <OtpComp
      propsKodeOtp={kodeOtp}
      propsKirimUlang={onClickKirimUlangOtp}
      propsVerifikasiOtp={onClickVerifikasiOtp}
    />
  );
}

export default OtpPage;
