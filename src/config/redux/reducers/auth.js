const init = {
  id: '',
  phone_number: '',
  token: '',
};

export default (state = init, { type, payload }) => {
  switch (type) {
    case 'LOGIN_SUCCESS':
      return {
        ...state,
        id: payload.id ? payload.id : state.id,
        phone_number: payload.phone_number,
        token: payload.token ? payload.token : state.token,
      };
    case 'LOGOUT_SUCCESS':
      return {
        ...state,
        id: '',
        phone_number: '',
        token: '',
      };

    default:
      return state;
  }
};
