export let loginAction = (data) => {
  localStorage.setItem('user', JSON.stringify(data));
  return { type: 'LOGIN_SUCCESS', payload: data };
};

export let logoutAction = () => {
  localStorage.removeItem('user');

  return {
    type: 'LOGOUT_SUCCESS',
  };
};
