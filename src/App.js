import './App.css';
import './style/style.css';
import { Route, BrowserRouter } from 'react-router-dom';

import HomeNonLoginPage from './page/HomeNonLoginPage';
import HomePage from './page/HomePage';
import LoginPage from './page/LoginPage';
import RegisterPage from './page/RegisterPage';
import OtpPage from './page/OtpPage';
import EditProfilePage from './page/EditProfilePage';
import EditRiwayatKerjaPage from './page/EditRiwayatKerjaPage';
import EditRiwayatPendidikanPage from './page/EditRiwayatPendidikanPage';
import AddRiwayatPage from './page/AddRiwayatPage';
import AddGalleryPage from './page/AddGalleryPage';

function App() {
  return (
    <BrowserRouter>
      <div className="app">
        <Route path="/" exact component={HomePage} />
        <Route path="/homenonlogin" exact component={HomeNonLoginPage} />
        <Route path="/login" component={LoginPage} />
        <Route path="/register" component={RegisterPage} />
        <Route path="/otp" component={OtpPage} />
        <Route path="/editprofile" component={EditProfilePage} />
        <Route
          path="/editriwayatkerja/:editriwayatkerja_id"
          component={EditRiwayatKerjaPage}
        />
        <Route
          path="/editriwayatpendidikan/:editriwayatpendidikan_id"
          component={EditRiwayatPendidikanPage}
        />
        <Route path="/AddRiwayatPage/:id" component={AddRiwayatPage} />
        <Route path="/AddGalleryPage" component={AddGalleryPage} />
      </div>
    </BrowserRouter>
  );
}

export default App;
